from kivy.app import App
from kivy.config import Config
from kivy.core.window import Window
from kivy.lang import Builder
from kivy.properties import ObjectProperty
from kivy.uix.scrollview import ScrollView

from Libs.uix.garden.navigationdrawer import NavigationDrawer
from kivymd.theming import ThemeManager
from kivymd.navigationdrawer import NavigationDrawerIconButton
from kivymd.label import MDLabel
from kivy.uix.boxlayout import BoxLayout

main_widget_kv = '''
#:import Toolbar kivymd.toolbar.Toolbar
#:import MDCard kivymd.card.MDCard
#:import SmartTile kivymd.grid.SmartTile
#:import MDDropdownMenu kivymd.menu.MDDropdownMenu

BoxLayout:
    orientation: 'vertical'
    Toolbar:
        id: toolbar
        #title: 'Алиф Капитал'
        title_theme_color: 'Secondary'
        background_color: app.theme_cls.primary_color
        left_action_items: [['menu', lambda x: app.nav_drawer.toggle_state()]]

    ScreenManager:
        id: scr_manager

        Screen:
            name: 'clients'

        Screen:
            name: 'statics'

        Screen:
            name: 'investors'

        Screen:
            name: 'leaders'
            ScrollView:
                do_scroll_x: False
                GridLayout:
                    cols: 2
                    row_default_height: (self.width - self.cols*self.spacing[0])/self.cols
                    row_force_default: True
                    size_hint_y: None
                    height: 8 * dp(100) # /1 * self.row_default_height
                    padding: dp(4), dp(4)
                    spacing: dp(4)
                    SmartTileWithLabel:
                        mipmap: True
                        source: 'Data/Images/man-02.jpg'
                        text: "АБДУЛЛО КУРБАНОВ"
                        on_release: MDDropdownMenu(items=app.kurbanov, width_mult=4).open(self)

                    SmartTileWithLabel:
                        mipmap: True
                        source: 'Data/Images/man-01.jpg'
                        text: "ЗУХУРШО РАХМАТУЛЛОЕВ"
                        on_release: MDDropdownMenu(items=app.rahmatulloev, width_mult=4).open(self)

                    SmartTileWithLabel:
                        mipmap: True
                        source: 'Data/Images/man-03.jpg'
                        text: "ФИРДАВС МИРЗОЕВ"
                        on_release: MDDropdownMenu(items=app.mirzoev, width_mult=4).open(self)
        Screen:
            name: 'request'

        Screen:
            name: 'about'
            ScrollView:
                do_scroll_x: False
                BoxLayout:
                    orientation: 'vertical'
                    size_hint_y: None
                    height: dp(550)
                    Image:
                        source: 'Data/Images/logo(1).png'
                    MDLabel:
                        font_style: 'Title'
                        theme_text_color: 'Primary'
                        text: app.about_us
                        halign: 'center'
                        size_hint_y: None
                        height: self.texture_size[1] + dp(4)


<AlifNavDrawer>
    title: 'Alif'
    BoxLayout:
        orientation: 'vertical'
        size_hint_y: None
        height: 352
        Toolbar:
            id: toolbar
            title: 'Алиф Капитал'
            title_theme_color: 'Secondary'
            background_color: app.theme_cls.primary_color
        NavigationDrawerIconButton:
            icon: 'account-card-details'
            text: "Клиентам"
            on_release: 1

        NavigationDrawerIconButton:
            icon: 'database'
            text: "Статистики"

        NavigationDrawerIconButton:
            icon: 'account-circle'
            text: "Инвесторам"

        NavigationDrawerIconButton:
            icon: 'account-multiple'
            text: "Руководство"
            on_release: app.main_widget.ids.scr_manager.current = 'leaders'; app.nav_drawer.toggle_state()

        NavigationDrawerIconButton:
            icon: 'message-outline'

            text: "Завяка"

        NavigationDrawerIconButton:
            icon: 'information'
            text: "О нас"
            on_release: app.main_widget.ids.scr_manager.current = 'about'; app.nav_drawer.toggle_state()
'''


class AlifNavDrawer(ScrollView):
    pass


class Program(App):
    theme_cls = ThemeManager()
    nav_drawer = ObjectProperty()

    about_us = '''
    Доступ к качественным финансовым услугам является одним из \
    ключевых локомотивов экономического роста. Алиф Капитал был \
    основан в Феврале 2014 года для предоставления финансовых решений \
    малому и среднему бизнесу в Республике Таджикистан.
    '''
    about_us=''

    Kurbanov = '''
    АБДУЛЛО КУРБАНОВ
        Опыт работы
        В прошлом:
        - Заместитель председателя | Ориёнбанк | Таджикистан
        - Вице президент по инвестициям | Origo Partners | Монголия
        - Аналитик | Инвестиционный банк UBS | Великобритания
        - Консультант | Oliver Wyman | Великобритания
        - Аналитик-интерн | Goldman Sachs | Великобритания
        Примеры проектов:
        - Модель системы оценки рисков кредитного портфеля для одного из самых крупных банков России.
        - Обратное слияние компании на Монгольской фондовой бирже
        - Реструктуризация портфеля активов международной горнодобывающей компании
        Образование:
        - London School of Economics (LSE), Финансы | Великобритания
        - Boğaziçi University, Менеджмент | Турция
    '''

    Rahmatulloev = '''
    ЗУХУРШО РАХМАТУЛЛОЕВ
        Опыт работы
        В настоящем:
        - Соучредитель | Риелторское агенство «Маскан» | Таджикистан
        - Соучредитель и бывший Председатель | Общественная организация «Пешрафт» | Таджикистан
        В прошлом:
        - Менеджер по развитию бизнеса и Исполнительный директор BDO консалтинг | BDO | Таджикистан
        - Интерн | Ориёнбанк | Таджикистан
        Образование:
        - International Center For Education in Islamic Finance (INCEIF), Сертифицированный Эксперт в сфере Исламских Финансов | Малазия
        - University of Warwick, Международная Политическая Экономика | Великобритания
        - London South Bank University (LSBU), Бизнес Администрирование | Великобритания
        '''

    Mirzoev = '''
    ФИРДАВС МИРЗОЕВ
        Опыт работы
        В настоящем:
        - Партнер | Назришо & Мирзоев | Таджикистан
        - Почетный советник по правовым вопросам | Посольство Великобритании | Таджикистан
        Опыт работы - в прошлом:
        - Юридический консультант | Tethys Services | Таджикистан
        - Независимый юридический эксперт | Консалтинговая фирма Aiten | Таджикистан
        Примеры проектов:
        - Соглашение с Китайским Банком Развития для кредита в размере $80 млн на строительство Китайско-Таджикского цементного завода
        - Инвестиции в размере $400 млн для строительства ТЭЦ Малайзийской компанией HOS Powertech International SDN BHD
        Образование:
        - Университет Сент-Луис, Юриспруденция | Миссури, США
        - Таджикский Национальный Университет, Юриспруденция | Таджикистан
    '''

    kurbanov = [
        {'viewclass': 'MDMenuItem',
         'text': ''},
        {'viewclass': 'MDMenuItem',
         'text': ''},
        {'viewclass': 'MDMenuItem',
         'text': ''},
        {'viewclass': 'MDMenuItem',
         'text': ''},
        {'viewclass': 'MDMenuItem',
         'text': ''},
        {'viewclass': 'MDMenuItem',
         'text': Kurbanov},
        {'viewclass': 'MDMenuItem',
         'text': ''},
        {'viewclass': 'MDMenuItem',
         'text': ''},
        {'viewclass': 'MDMenuItem',
         'text': ''},
        {'viewclass': 'MDMenuItem',
         'text': ''},
    ]

    rahmatulloev = [
        {'viewclass': 'MDMenuItem',
         'text': ''},
        {'viewclass': 'MDMenuItem',
         'text': ''},
        {'viewclass': 'MDMenuItem',
         'text': ''},
        {'viewclass': 'MDMenuItem',
         'text': ''},
        {'viewclass': 'MDMenuItem',
         'text': ''},
        {'viewclass': 'MDMenuItem',
         'text': Rahmatulloev},
        {'viewclass': 'MDMenuItem',
         'text': ''},
        {'viewclass': 'MDMenuItem',
         'text': ''},
        {'viewclass': 'MDMenuItem',
         'text': ''},
        {'viewclass': 'MDMenuItem',
         'text': ''},
    ]

    mirzoev = [
        {'viewclass': 'MDMenuItem',
         'text': ''},
        {'viewclass': 'MDMenuItem',
         'text': ''},
        {'viewclass': 'MDMenuItem',
         'text': ''},
        {'viewclass': 'MDMenuItem',
         'text': ''},
        {'viewclass': 'MDMenuItem',
         'text': ''},
        {'viewclass': 'MDMenuItem',
         'text': Mirzoev},
        {'viewclass': 'MDMenuItem',
         'text': ''},
        {'viewclass': 'MDMenuItem',
         'text': ''},
        {'viewclass': 'MDMenuItem',
         'text': ''},
        {'viewclass': 'MDMenuItem',
         'text': ''},
        {'viewclass': 'MDMenuItem',
         'text': ''}
    ]

    def __init__(self, **kwargs):
        super(Program, self).__init__(**kwargs)
        # Window.bind(on_keyboard=self.events_program)

    def build(self):
        self.main_widget = Builder.load_string(main_widget_kv)
        self.main_widget.ids.scr_manager.current = 'about'
        # Box1 = BoxLayout(orientation='vertical').add_widget(AlifNavDrawer())
        self.theme_cls.theme_style = 'Dark'
        self.nav_drawer = NavigationDrawer()
        self.nav_drawer.add_widget(AlifNavDrawer())
        self.nav_drawer.anim_type = 'fade_in'
        # self.nav_drawer.anim_type = 'slide_above_anim'
        self.nav_drawer.add_widget(self.main_widget)
        self.icon = 'Data/Images/favicon.ico'
        self.title = 'Алиф'

        return self.nav_drawer
